#! /usr/bin/perl -w

use strict;
use Socket;

#   **** default port; ****
use constant SIMPLE_UDP_PORT => 4001;
#   **** max data that can be received; ****
use constant MAX_RECV_LEN => 1500;
#   **** default host to connect to; ****
use constant REMOTE_HOST => '192.168.1.167';
#   **** take in command arg or default value for ip addr; ****
my $remote = shift || REMOTE_HOST;
#   **** get port (service) number; ****
my $trans_serv = getprotobyname('udp');
#   **** use $remote host to get usable ip to put in sockaddr_in; ****
my $remote_host = gethostbyname($remote) or
  die "udp_c: name lookup failed: $remote\n";
#   **** take in command arg or use default port number; ****
my $remote_port = shift || SIMPLE_UDP_PORT;
#   **** make a socket port pair to be used to create a socket; ****
my $destination = sockaddr_in($remote_port, $remote_host);
#   **** create the actual socket!!! ****
socket(UDP_SOCK, PF_INET, SOCK_DGRAM, $trans_serv) or
  die "error: $!";
#   **** data to be sent on the wire; ****
my $data = "This is a simple UDP message";
#   **** server's main operation in here; ****
while(1){
    #   **** sends udp datagram to predefined socket structure; ****
    send(UDP_SOCK, $data, 0, $destination) or
      die "error: $!";
    #   **** give time to check for fail to send; ****
    sleep(2);
#   **** start creating a forked process; ****
no strict 'vars';
# **** is a $child_pid already defined; ****
if (defined($child_pid))
{
  #   **** kill $child_pid if it is defined; ****
  if (kill 0 => $child_pid)
  {
    next;
  }
}
#   **** create the child if it is defined; ****
$child_pid = fork;
#   **** is $child_pid have actual data; ****
if ($child_pid)
{
  next;
}
#   **** if $child_pid is defined lets fork; ****
elsif (defined($child_pid))
  {
    #   **** create signal interupt for connect fail alarm; ****
    $SIG{ALRM} = sub {die "recv timeout\n";};
    #   **** start alarm; ****
    alarm(5);
    #   **** block that must complete before alarm ends; ****
    eval{
      #   **** variable to hold recv() return data; ****
      my $from_who = recv( UDP_SOCK, $data, MAX_RECV_LEN, 0 );

      #   **** check if there is any data from recv(); ****
      if ($from_who){
        #   **** create socket port pair; ****
        my ($the_port, $the_ip) = sockaddr_in($from_who);
        #   **** receive port and ip address of the recv() data; ****
        my $remote_name = gethostbyaddr($the_ip, AF_INET);
        #   **** print out the data from recv(); ****
        warn "Received from $remote_name: ", length($data),
          "$data\n";
        }
        #   **** if if($from_who) fails; ****
        else{
          warn "problem with recv $!\n";
        }
        #   **** alarm ends ****
        alarm(0);
        #   **** block under the alarm closing brace; ****
      };
      # **** performed if alarm is true; ****
      if($@)
      {
        #   **** alarm failed and connection wasn't in time limit; ****
        die "$@\n" unless $@ =~ /recv timeout/;
        warn "recv timed out, canceling\n";
      }
      exit
 }

}
# **** close the socket, probably will never be reached; ****
close UDP_SOCK
  or die "close socket failed: $!\n";
