#! /usr/bin/perl -w
use strict;
use Socket;

#   *** default port to use; ***
use constant SIMPLE_UDP_PORT => 4001;
#   *** default data recv size; ***
use constant MAX_RECV_LEN => 1500;
#   *** default host ip, this allows for portability; ***
use constant LOCAL_INETNAME => 'localhost';
#   *** get type of protocol udp in this case; ***
my $trans_serv = getprotobyname( 'udp' );
#   *** use default ip; ***
my $local_host = gethostbyname( LOCAL_INETNAME );
#   *** use command arg for port num or default; ***
my $local_port = shift || SIMPLE_UDP_PORT;
#   *** create socket port pair to be used; ***
my $local_addr = sockaddr_in( $local_port, INADDR_ANY );
#   *** actually create the socket; ***
socket( UDP_SOCK, PF_INET, SOCK_DGRAM, $trans_serv ) or
  die "udp_s: socket creation failed: $!\n";
#   *** bind the socket to the label UDP_SOCK; ***
bind( UDP_SOCK, $local_addr ) or
  die "udp_s: bind to address failed: $!\n";

#   *** what will hold our message for the wire; ***
my $data;
#   *** main operation of the server happens here; ***
while( 1 )
{
  #   *** recv data from a client that has connected; ***
  my $from_who = recv( UDP_SOCK, $data, MAX_RECV_LEN, 0 );
  #   *** is there any data in $from_who; ***
  if ( $from_who )
  {
    #   *** return port number and ip address from connector; ***
    my ( $the_port, $the_ip ) = sockaddr_in( $from_who );
    #   *** convert number ip to readable ip; ***
    my $remote_name = gethostbyaddr($the_ip, AF_INET);
    #   *** print out who sent the packet; ***
    warn 'Received from ', inet_ntoa( $the_ip ), ": $data\n";
  }
  #   *** give us time to send the reply; ***
  sleep(1);
  #   *** send the reply to the person who connected; ***
  send (UDP_SOCK, $data, 0, $from_who) or
    warn "send to socket failed $! \n";
}
